package com.bbank.vo;

import java.sql.Blob;
import java.util.List;

import com.bbank.model.DonorBloodunit;

public class DonerProfileUIVO {
	
	private Integer donorid;
	
	private Integer uid;

	private String middle_name;
	
	private String lastname;
		
	private String firstName;

	private String fatherName;
	
	private String dOB;
	
	private long mobile;
	
	private String bloodGroup;

	private String email;
	
	private String address;
	
	private String city;
	
	private String country;
	
	private String pinCode;
	
	private String proofId_typ;
	
	private String proof_id;
	
	private String state;
	
	private Blob image;

	private List<DonorBloodunitVO> donorBloodunituivo;

	public Integer getDonorid() {
		return donorid;
	}

	public void setDonorid(Integer donorid) {
		this.donorid = donorid;
	}

	public Integer getUid() {
		return uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getdOB() {
		return dOB;
	}

	public void setdOB(String dOB) {
		this.dOB = dOB;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getProofId_typ() {
		return proofId_typ;
	}

	public void setProofId_typ(String proofId_typ) {
		this.proofId_typ = proofId_typ;
	}

	public String getProof_id() {
		return proof_id;
	}

	public void setProof_id(String proof_id) {
		this.proof_id = proof_id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public List<DonorBloodunitVO> getDonorBloodunituivo() {
		return donorBloodunituivo;
	}

	public void setDonorBloodunituivo(List<DonorBloodunitVO> donorBloodunituivo) {
		this.donorBloodunituivo = donorBloodunituivo;
	}

	
	
}
