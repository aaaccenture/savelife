package com.bbank.vo;

import java.util.Date;
import java.util.List;

import com.bbank.model.DonerProfile;
import com.bbank.model.HospitalData;
import com.bbank.model.Hosptlbldinvdtl;
import com.bbank.model.User;

public class DonorBloodunitVO {

	
	private Integer bloodid;

	private String donation_dt;

	private String expiry_dt;

	private String blood_report;

	private String reimbursed;
	
	private DonerProfileVO donerProfileVO;
	
	private HospitalDataVO hospitalDatavo;

	private HosptlbldinvdtlVO hosptlbldinvdtlVO;
	

	public Integer getBloodid() {
		return bloodid;
	}

	public void setBloodid(Integer bloodid) {
		this.bloodid = bloodid;
	}

	
	public String getDonation_dt() {
		return donation_dt;
	}

	public void setDonation_dt(String donation_dt) {
		this.donation_dt = donation_dt;
	}

	public String getExpiry_dt() {
		return expiry_dt;
	}

	public void setExpiry_dt(String expiry_dt) {
		this.expiry_dt = expiry_dt;
	}

	public String getBlood_report() {
		return blood_report;
	}

	public void setBlood_report(String blood_report) {
		this.blood_report = blood_report;
	}

	public String getReimbursed() {
		return reimbursed;
	}

	public void setReimbursed(String reimbursed) {
		this.reimbursed = reimbursed;
	}


	public HosptlbldinvdtlVO getHosptlbldinvdtlVO() {
		return hosptlbldinvdtlVO;
	}

	public void setHosptlbldinvdtlVO(HosptlbldinvdtlVO hosptlbldinvdtlVO) {
		this.hosptlbldinvdtlVO = hosptlbldinvdtlVO;
	}

	public DonerProfileVO getDonerProfileVO() {
		return donerProfileVO;
	}

	public void setDonerProfileVO(DonerProfileVO donerProfileVO) {
		this.donerProfileVO = donerProfileVO;
	}

	public HospitalDataVO getHospitalDatavo() {
		return hospitalDatavo;
	}

	public void setHospitalDatavo(HospitalDataVO hospitalDatavo) {
		this.hospitalDatavo = hospitalDatavo;
	}

	
		
}
