package com.bbank.vo;

import java.sql.Blob;
import java.util.List;
import java.util.Set;

import com.bbank.model.DonorBloodunit;
import com.bbank.model.Hospitalbldunit;

public class HospitalDataUIVO {
	
	private Integer hospital_id;

	private String name;

	private String organization;

	private String address;

	private String city;

	private String state;

	private String country;

	private String pinCode;

	private int landLineNumber;

	private int directDial;

	private String uRL;

	private String pOC;

	private Blob image;
	
	private UserVO uservo;
	
	private Set<HospitalbldunitVO> hospitalbldunitVO;
	
	private List<DonorBloodunitVO> donorBloodunitVO;

	public Integer getHospital_id() {
		return hospital_id;
	}

	public void setHospital_id(Integer hospital_id) {
		this.hospital_id = hospital_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public int getLandLineNumber() {
		return landLineNumber;
	}

	public void setLandLineNumber(int landLineNumber) {
		this.landLineNumber = landLineNumber;
	}

	public int getDirectDial() {
		return directDial;
	}

	public void setDirectDial(int directDial) {
		this.directDial = directDial;
	}

	public String getuRL() {
		return uRL;
	}

	public void setuRL(String uRL) {
		this.uRL = uRL;
	}

	public String getpOC() {
		return pOC;
	}

	public void setpOC(String pOC) {
		this.pOC = pOC;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public UserVO getUservo() {
		return uservo;
	}

	public void setUservo(UserVO uservo) {
		this.uservo = uservo;
	}

	public Set<HospitalbldunitVO> getHospitalbldunitVO() {
		return hospitalbldunitVO;
	}

	public void setHospitalbldunitVO(Set<HospitalbldunitVO> hospitalbldunitVO) {
		this.hospitalbldunitVO = hospitalbldunitVO;
	}

	public List<DonorBloodunitVO> getDonorBloodunitVO() {
		return donorBloodunitVO;
	}

	public void setDonorBloodunitVO(List<DonorBloodunitVO> donorBloodunitVO) {
		this.donorBloodunitVO = donorBloodunitVO;
	}

	
	
}
