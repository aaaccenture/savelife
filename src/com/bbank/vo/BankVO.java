package com.bbank.vo;




public class BankVO  {

	
	private int bid;
	private String bankname;	
	private String zone;	
	private String state;	
	private String branch;	
	private String ifsccode;
	private String producttype;
	private String creditscore;
	private long intrestrate;
	private long processingfee;
	private long prepaymentfee;

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getIfsccode() {
		return ifsccode;
	}

	public void setIfsccode(String ifsccode) {
		this.ifsccode = ifsccode;
	}

	public String getProducttype() {
		return producttype;
	}

	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}

	public String getCreditscore() {
		return creditscore;
	}

	public void setCreditscore(String creditscore) {
		this.creditscore = creditscore;
	}

	public long getIntrestrate() {
		return intrestrate;
	}

	public void setIntrestrate(long intrestrate) {
		this.intrestrate = intrestrate;
	}

	public long getProcessingfee() {
		return processingfee;
	}

	public void setProcessingfee(long processingfee) {
		this.processingfee = processingfee;
	}

	public long getPrepaymentfee() {
		return prepaymentfee;
	}

	public void setPrepaymentfee(long prepaymentfee) {
		this.prepaymentfee = prepaymentfee;
	}

}
