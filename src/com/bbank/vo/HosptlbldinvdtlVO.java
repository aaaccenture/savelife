package com.bbank.vo;

import com.bbank.model.DonerProfile;
import com.bbank.model.DonorBloodunit;
import com.bbank.model.HospitalData;

public class HosptlbldinvdtlVO {
	
	private int bloodid;

	private String blood_lot_number;

	private String blood_group;
	
	private HospitalData hospitalData;
	
	private DonerProfile donerProfile;
	
	private DonorBloodunitVO donorBloodunitVO;

	public int getBloodid() {
		return bloodid;
	}

	public void setBloodid(int bloodid) {
		this.bloodid = bloodid;
	}

	public String getBlood_lot_number() {
		return blood_lot_number;
	}

	public void setBlood_lot_number(String blood_lot_number) {
		this.blood_lot_number = blood_lot_number;
	}

	public String getBlood_group() {
		return blood_group;
	}

	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}

	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}

	public DonerProfile getDonerProfile() {
		return donerProfile;
	}

	public void setDonerProfile(DonerProfile donerProfile) {
		this.donerProfile = donerProfile;
	}

	public DonorBloodunitVO getDonorBloodunitVO() {
		return donorBloodunitVO;
	}

	public void setDonorBloodunitVO(DonorBloodunitVO donorBloodunitVO) {
		this.donorBloodunitVO = donorBloodunitVO;
	}



}
