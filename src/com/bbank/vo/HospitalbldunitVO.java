package com.bbank.vo;

import com.bbank.model.HospitalData;

public class HospitalbldunitVO {
	
	private Integer id;
	private String blood_group;
	private int unit_count;
	
	private HospitalDataVO hospitalDatavo;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getBlood_group() {
		return blood_group;
	}
	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}
	public int getUnit_count() {
		return unit_count;
	}
	public void setUnit_count(int unit_count) {
		this.unit_count = unit_count;
	}
	public HospitalDataVO getHospitalDatavo() {
		return hospitalDatavo;
	}
	public void setHospitalDatavo(HospitalDataVO hospitalDatavo) {
		this.hospitalDatavo = hospitalDatavo;
	}
	
	
	}
