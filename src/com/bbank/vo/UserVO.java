package com.bbank.vo;



public class UserVO {

	private Integer id;

	private String username;
	
	private String password;
		
	private String usertype;
	
	private String group;
	
    private boolean enabled;
    
	
	private DonerProfileVO donerProfilevo;
	
	private HospitalDataVO hospitalDatavo;
	
	private BloodDetailVO bloodDetailvo;
	
	private DonorBloodunitVO donorBloodunitvo;
	
	

	public String getGroup() {
		return group;
	}


	public void setGroup(String group) {
		this.group = group;
	}


		
	public DonerProfileVO getDonerProfilevo() {
		return donerProfilevo;
	}


	public void setDonerProfilevo(DonerProfileVO donerProfilevo) {
		this.donerProfilevo = donerProfilevo;
	}

	
	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	
	public HospitalDataVO getHospitalDatavo() {
		return hospitalDatavo;
	}


	public void setHospitalDatavo(HospitalDataVO hospitalDatavo) {
		this.hospitalDatavo = hospitalDatavo;
	}


	public BloodDetailVO getBloodDetailvo() {
		return bloodDetailvo;
	}


	public void setBloodDetailvo(BloodDetailVO bloodDetailvo) {
		this.bloodDetailvo = bloodDetailvo;
	}


	public DonorBloodunitVO getDonorBloodunitvo() {
		return donorBloodunitvo;
	}


	public void setDonorBloodunitvo(DonorBloodunitVO donorBloodunitvo) {
		this.donorBloodunitvo = donorBloodunitvo;
	}

	
}
