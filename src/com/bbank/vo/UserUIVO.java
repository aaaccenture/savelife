package com.bbank.vo;

public class UserUIVO {
	
	private int donoid;
	private int hospitlid;
	private int uid;
	private String name;
	private String username;
	private String userType;
	private String groupame;

	public int getDonoid() {
		return donoid;
	}

	public void setDonoid(int donoid) {
		this.donoid = donoid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public int getHospitlid() {
		return hospitlid;
	}

	public void setHospitlid(int hospitlid) {
		this.hospitlid = hospitlid;
	}

	public String getGroupame() {
		return groupame;
	}

	public void setGroupame(String groupame) {
		this.groupame = groupame;
	}
	

}
