package com.bbank.vo;

import com.bbank.model.User;

public class BloodDetailVO {
	
	private Integer blooddtlid;
	private String wBC;

	private String rBC;

	private String hB;

	private String rhFactor;

	private String platelets;

	private String lymphocytes;

	private String mCV;
	
	private User user;

	public Integer getBlooddtlid() {
		return blooddtlid;
	}

	public void setBlooddtlid(Integer blooddtlid) {
		this.blooddtlid = blooddtlid;
	}

	public String getwBC() {
		return wBC;
	}

	public void setwBC(String wBC) {
		this.wBC = wBC;
	}

	public String getrBC() {
		return rBC;
	}

	public void setrBC(String rBC) {
		this.rBC = rBC;
	}

	public String gethB() {
		return hB;
	}

	public void sethB(String hB) {
		this.hB = hB;
	}

	public String getRhFactor() {
		return rhFactor;
	}

	public void setRhFactor(String rhFactor) {
		this.rhFactor = rhFactor;
	}

	public String getPlatelets() {
		return platelets;
	}

	public void setPlatelets(String platelets) {
		this.platelets = platelets;
	}

	public String getLymphocytes() {
		return lymphocytes;
	}

	public void setLymphocytes(String lymphocytes) {
		this.lymphocytes = lymphocytes;
	}

	public String getmCV() {
		return mCV;
	}

	public void setmCV(String mCV) {
		this.mCV = mCV;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

}
