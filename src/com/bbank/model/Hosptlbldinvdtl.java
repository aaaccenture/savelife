package com.bbank.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="hosptl_bld_inv_dtl_tbl")
public class Hosptlbldinvdtl implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="bloodinvid")
	private int bloodinvid;

	@Column(name="Blood_lot_number")
	private String blood_lot_number;

	@Column(name="Blood_group")
	private String blood_group;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="Hospital_id", nullable=false)
	private HospitalData hospitalData;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="donorid", nullable=false)
	private DonerProfile donerProfile;
	
	@OneToOne(mappedBy="hosptlbldinvdtl")
	private DonorBloodunit donorBloodunit;

	
	public int getBloodinvid() {
		return bloodinvid;
	}

	public void setBloodinvid(int bloodinvid) {
		this.bloodinvid = bloodinvid;
	}

	public String getBlood_lot_number() {
		return blood_lot_number;
	}

	public void setBlood_lot_number(String blood_lot_number) {
		this.blood_lot_number = blood_lot_number;
	}

	public String getBlood_group() {
		return blood_group;
	}

	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}

	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}

	public DonerProfile getDonerProfile() {
		return donerProfile;
	}

	public void setDonerProfile(DonerProfile donerProfile) {
		this.donerProfile = donerProfile;
	}

	public DonorBloodunit getDonorBloodunit() {
		return donorBloodunit;
	}

	public void setDonorBloodunit(DonorBloodunit donorBloodunit) {
		this.donorBloodunit = donorBloodunit;
	}
		
	
}


