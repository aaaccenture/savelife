package com.bbank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="blood_request_tbl")
public class BloodRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="requestid")
	private Integer requestid;

	@Column(name="request_dt")
	private Date request_dt;


	@Column(name="unit_cnt")
	private int unit_cnt;

	@Column(name="comments")
	private String comments;

	@Column(name="urgency")
	private int urgency;

	@Column(name="notificationid")
	private int notificationid;
}

