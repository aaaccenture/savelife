package com.bbank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="notification_tbl")
public class Notification implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="notificationId")
	private Integer notificationId;

	@Column(name="notificationTyp")
	private String notificationTyp;

	@Column(name="notification_dt")
	private Date notification_dt;

	@Column(name="message")
	private String message;
}

