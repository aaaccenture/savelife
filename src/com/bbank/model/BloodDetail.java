package com.bbank.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;@Entity
@Table(name="blood_detail_tbl")
public class BloodDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="blooddtlid")
	private Integer blooddtlid;

	@Column(name="WBC")
	private String wBC;

	@Column(name="RBC")
	private String rBC;

	@Column(name="HB")
	private String hB;

	@Column(name="RhFactor")
	private String rhFactor;

	@Column(name="Platelets")
	private String platelets;

	@Column(name="Lymphocytes")
	private String lymphocytes;

	@Column(name="MCV")
	private String mCV;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_id")
	private User user;

	public Integer getBlooddtlid() {
		return blooddtlid;
	}

	public void setBlooddtlid(Integer blooddtlid) {
		this.blooddtlid = blooddtlid;
	}

	public String getwBC() {
		return wBC;
	}

	public void setwBC(String wBC) {
		this.wBC = wBC;
	}

	public String getrBC() {
		return rBC;
	}

	public void setrBC(String rBC) {
		this.rBC = rBC;
	}

	public String gethB() {
		return hB;
	}

	public void sethB(String hB) {
		this.hB = hB;
	}

	public String getRhFactor() {
		return rhFactor;
	}

	public void setRhFactor(String rhFactor) {
		this.rhFactor = rhFactor;
	}

	public String getPlatelets() {
		return platelets;
	}

	public void setPlatelets(String platelets) {
		this.platelets = platelets;
	}

	public String getLymphocytes() {
		return lymphocytes;
	}

	public void setLymphocytes(String lymphocytes) {
		this.lymphocytes = lymphocytes;
	}

	public String getmCV() {
		return mCV;
	}

	public void setmCV(String mCV) {
		this.mCV = mCV;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
}

