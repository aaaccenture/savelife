package com.bbank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="donor_blood_unit_tbl")
public class DonorBloodunit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Bloodid")
	private Integer bloodid;

	@Column(name="Donation_dt")
	private String donation_dt;

	@Column(name="Expiry_dt")
	private String expiry_dt;

	@Column(name="Blood_report")
	private String blood_report;

	@Column(name="Reimbursed")
	private String reimbursed;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="donorid", nullable=false)
	private DonerProfile donerProfile;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="Hospital_id", nullable=false)
	private HospitalData hospitalData;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "bloodinvid")
	private Hosptlbldinvdtl hosptlbldinvdtl;
	
	public DonorBloodunit() {
		
		// TODO Auto-generated constructor stub
	}

	public DonorBloodunit(Integer bloodid, String donation_dt, String expiry_dt, String blood_report, String reimbursed,
			DonerProfile donerProfile,HospitalData hospitalData) {
		this.bloodid = bloodid;
		this.donation_dt = donation_dt;
		this.expiry_dt = expiry_dt;
		this.blood_report = blood_report;
		this.reimbursed = reimbursed;
		this.donerProfile = donerProfile;
		this.hospitalData = hospitalData;
	}

	public Integer getBloodid() {
		return bloodid;
	}

	public void setBloodid(Integer bloodid) {
		this.bloodid = bloodid;
	}

	
	public String getDonation_dt() {
		return donation_dt;
	}

	public void setDonation_dt(String donation_dt) {
		this.donation_dt = donation_dt;
	}

	public String getExpiry_dt() {
		return expiry_dt;
	}

	public void setExpiry_dt(String expiry_dt) {
		this.expiry_dt = expiry_dt;
	}

	public String getBlood_report() {
		return blood_report;
	}

	public void setBlood_report(String blood_report) {
		this.blood_report = blood_report;
	}

	public String getReimbursed() {
		return reimbursed;
	}

	public void setReimbursed(String reimbursed) {
		this.reimbursed = reimbursed;
	}

	

	public DonerProfile getDonerProfile() {
		return donerProfile;
	}

	public void setDonerProfile(DonerProfile donerProfile) {
		this.donerProfile = donerProfile;
	}

	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}

	public Hosptlbldinvdtl getHosptlbldinvdtl() {
		return hosptlbldinvdtl;
	}

	public void setHosptlbldinvdtl(Hosptlbldinvdtl hosptlbldinvdtl) {
		this.hosptlbldinvdtl = hosptlbldinvdtl;
	}	
	
	
}

