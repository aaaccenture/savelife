package com.bbank.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="hospital_data_tbl")
public class HospitalData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Hospital_id")
	private Integer hospital_id;

	@Column(name="Name")
	private String name;

	@Column(name="Organization")
	private String organization;

	@Column(name="Address")
	private String address;

	@Column(name="City")
	private String city;

	@Column(name="State")
	private String state;

	@Column(name="Country")
	private String country;

	@Column(name="PinCode")
	private String pinCode;

	@Column(name="LandLineNumber")
	private int landLineNumber;

	@Column(name="DirectDial")
	private int directDial;

	@Column(name="URL")
	private String uRL;

	@Column(name="POC")
	private String pOC;

	@Column(name="Image")
	private Blob image;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "uid")
	private User user;
	
	@OneToMany(mappedBy="hospitalData",cascade = CascadeType.ALL)
	private List<DonorBloodunit> donorBloodunit;
	
	@OneToMany(mappedBy="hospitalData")
	private Set<Hospitalbldunit> hospitalbldunit;
	
	@OneToMany(mappedBy="hospitalData",cascade = CascadeType.ALL)
	private Set<Hosptlbldinvdtl> hosptlbldinvdtl;

	public Integer getHospital_id() {
		return hospital_id;
	}

	public void setHospital_id(Integer hospital_id) {
		this.hospital_id = hospital_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrganization() {
		return organization;
	}

	public void setOrganization(String organization) {
		this.organization = organization;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public int getLandLineNumber() {
		return landLineNumber;
	}

	public void setLandLineNumber(int landLineNumber) {
		this.landLineNumber = landLineNumber;
	}

	public int getDirectDial() {
		return directDial;
	}

	public void setDirectDial(int directDial) {
		this.directDial = directDial;
	}

	public String getuRL() {
		return uRL;
	}

	public void setuRL(String uRL) {
		this.uRL = uRL;
	}

	public String getpOC() {
		return pOC;
	}

	public void setpOC(String pOC) {
		this.pOC = pOC;
	}

	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Hospitalbldunit> getHospitalbldunit() {
		return hospitalbldunit;
	}

	public void setHospitalbldunit(Set<Hospitalbldunit> hospitalbldunit) {
		this.hospitalbldunit = hospitalbldunit;
	}

	public List<DonorBloodunit> getDonorBloodunit() {
		return donorBloodunit;
	}

	public void setDonorBloodunit(List<DonorBloodunit> donorBloodunit) {
		this.donorBloodunit = donorBloodunit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((hospital_id == null) ? 0 : hospital_id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HospitalData))
			return false;
		HospitalData other = (HospitalData) obj;
		if (hospital_id == null) {
			if (other.hospital_id != null)
				return false;
		} else if (!hospital_id.equals(other.hospital_id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HospitalData [hospital_id=" + hospital_id + ", name=" + name + "]";
	}


		
}

