package com.bbank.model;

import java.io.Serializable;
import java.sql.Blob;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Entity
@Table(name="donor_profile_tbl")
public class DonerProfile implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "donorid")
	private Integer donorid;

	@Column(name="Middle_name")
	private String middle_name;

	@Column(name="First_name")
	private String firstName;
	
	@Column(name="Last_name")
	private String lastname;

	@Column(name="Father_name")
	private String fatherName;
	
	@Column(name="DOB")
	private String dOB;

	@Column(name="Mobile")
	private long mobile;

	@Column(name="BloodGroup")
	private String bloodGroup;

	@Column(name="Email")
	private String email;

	@Column(name="Address")
	private String address;

	@Column(name="City")
	private String city;

	@Column(name="Country")
	private String country;

	@Column(name="PinCode")
	private String pinCode;

	@Column(name="ProofId_typ")
	private String proofId_typ;

	@Column(name="Proof_id")
	private String proof_id;

	@Column(name="State")
	private String state;


	@Column(name="Image")
	private Blob image;


	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "uid")
	private User user;
	
	@OneToMany(mappedBy="donerProfile",cascade = CascadeType.ALL)
	private List<DonorBloodunit> donorBloodunit;
	
	@OneToMany(mappedBy="donerProfile",cascade = CascadeType.ALL)
	private List<Hosptlbldinvdtl> hosptlbldinvdtl;

	
	public Integer getDonorid() {
		return donorid;
	}


	public void setDonorid(Integer donorid) {
		this.donorid = donorid;
	}


	public String getMiddle_name() {
		return middle_name;
	}


	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getFatherName() {
		return fatherName;
	}


	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}


	public String getdOB() {
		return dOB;
	}


	public void setdOB(String dOB) {
		this.dOB = dOB;
	}


	public String getBloodGroup() {
		return bloodGroup;
	}


	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public long getMobile() {
		return mobile;
	}


	public void setMobile(long mobile) {
		this.mobile = mobile;
	}


	public String getPinCode() {
		return pinCode;
	}


	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}


	public String getProofId_typ() {
		return proofId_typ;
	}


	public void setProofId_typ(String proofId_typ) {
		this.proofId_typ = proofId_typ;
	}


	public String getProof_id() {
		return proof_id;
	}


	public void setProof_id(String proof_id) {
		this.proof_id = proof_id;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public Blob getImage() {
		return image;
	}


	public void setImage(Blob image) {
		this.image = image;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public List<DonorBloodunit> getDonorBloodunit() {
		return donorBloodunit;
	}


	public void setDonorBloodunit(List<DonorBloodunit> donorBloodunit) {
		this.donorBloodunit = donorBloodunit;
	}


	public List<Hosptlbldinvdtl> getHosptlbldinvdtl() {
		return hosptlbldinvdtl;
	}


	public void setHosptlbldinvdtl(List<Hosptlbldinvdtl> hosptlbldinvdtl) {
		this.hosptlbldinvdtl = hosptlbldinvdtl;
	}


	


}
