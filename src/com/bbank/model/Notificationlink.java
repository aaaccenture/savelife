package com.bbank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="notification_link_tbl")
public class Notificationlink implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="linkid")
	private Integer linkid;

	@Column(name="notificationid")
	private int notificationid;

	@Column(name="donorid")
	private int donorid;

	@Column(name="Hospital_id")
	private int hospital_id;

	@Column(name="msg_publish_dt")
	private Date msg_publish_dt;
}

