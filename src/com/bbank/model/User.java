package com.bbank.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="credential_tbl")
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "uid")
	private Integer id;

    @Column(name="username")
	private String username;

	@Column(name="password")
	private String password;
	
	@Column(name="utype")
	private String usertype;

	@Column(name="enabled")
	private boolean enabled;
	
	@Column(name="groupname")
	private String group;
	
    @OneToOne(mappedBy="user")
	private DonerProfile donerProfile;
    
	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}

	@OneToOne(mappedBy="user")
	private HospitalData hospitalData;
	/*
	@OneToOne(mappedBy="user", cascade = CascadeType.ALL, orphanRemoval = true)
	private BloodDetail bloodDetail;
	
	@OneToOne(mappedBy="user", cascade = CascadeType.ALL, orphanRemoval = true)
	private DonorBloodunit donorBloodunit;*/
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public DonerProfile getDonerProfile() {
		return donerProfile;
	}

	public void setDonerProfile(DonerProfile donerProfile) {
		this.donerProfile = donerProfile;
	}
	/*
	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}

	public BloodDetail getBloodDetail() {
		return bloodDetail;
	}

	public void setBloodDetail(BloodDetail bloodDetail) {
		this.bloodDetail = bloodDetail;
	}

	public DonorBloodunit getDonorBloodunit() {
		return donorBloodunit;
	}

	public void setDonorBloodunit(DonorBloodunit donorBloodunit) {
		this.donorBloodunit = donorBloodunit;
	}*/
	
	

	
	}
