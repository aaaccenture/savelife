package com.bbank.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="hospital_bld_unit_tbl")
public class Hospitalbldunit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@Column(name="Blood_group")
	private String blood_group;

	@Column(name="Unit_count")
	private int unit_count;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="Hospital_id", nullable=false)
	private HospitalData hospitalData;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBlood_group() {
		return blood_group;
	}

	public void setBlood_group(String blood_group) {
		this.blood_group = blood_group;
	}

	public int getUnit_count() {
		return unit_count;
	}

	public void setUnit_count(int unit_count) {
		this.unit_count = unit_count;
	}

	public HospitalData getHospitalData() {
		return hospitalData;
	}

	public void setHospitalData(HospitalData hospitalData) {
		this.hospitalData = hospitalData;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((blood_group == null) ? 0 : blood_group.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Hospitalbldunit))
			return false;
		Hospitalbldunit other = (Hospitalbldunit) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (blood_group == null) {
			if (other.blood_group != null)
				return false;
		} else if (!blood_group.equals(other.blood_group))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Hospitalbldunit [id=" + id + ", blood_group=" + blood_group + "]";
	}
	
}

