package com.bbank.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="blood_rpt_tbl")
public class Bloodrpt implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="bloodrptid")
	private Integer bloodrptid;

	@Column(name="report_dt")
	private Date report_dt;

	@Column(name="HIV")
	private String HIV;

	@Column(name="Billirubin")
	private String Billirubin;

	@Column(name="HopatitisA")
	private String hopatitisA;

	@Column(name="HopatitisB")
	private String hopatitisB;

	@Column(name="HopatitisC")
	private String hopatitisC;
	
}

