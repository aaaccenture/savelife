package com.bbank.controller;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.bbank.model.DonorBloodunit;
import com.bbank.model.HospitalData;
import com.bbank.model.Hospitalbldunit;
import com.bbank.model.User;
import com.bbank.service.HospitalDataService;
import com.bbank.vo.DonerProfileVO;
import com.bbank.vo.DonorBloodunitVO;
import com.bbank.vo.HospitalDataUIVO;
import com.bbank.vo.HospitalDataVO;
import com.bbank.vo.HospitalbldunitVO;
import com.bbank.vo.UIStatusMessageVO;
import com.bbank.vo.UserVO;


@Controller
public class HospitalDataController {

	@Autowired
	private HospitalDataService hospitalDataService;


	@RequestMapping(value="/gethospitalData/{id}", method = RequestMethod.GET )
	public ResponseEntity<List<Object>> gethospitalDataById(@PathVariable("id") Integer id) {
		UIStatusMessageVO statusMessageVO= new UIStatusMessageVO();
		List<Object> list=new ArrayList<Object>();
		HospitalDataUIVO hospitalDatavo=null;
		try {
			hospitalDatavo = prepareHospitalDatavo(hospitalDataService.getHospitalDataById(id));
			list.add(hospitalDatavo);
			return new ResponseEntity<List<Object>>(list, HttpStatus.OK);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if(e.getMessage()!=null)
			{
				statusMessageVO.setStatus("failed");
				statusMessageVO.setMessage(e.getMessage());
				list.add(statusMessageVO);

			}else {
				statusMessageVO.setStatus("failed");
				statusMessageVO.setMessage("Hospital Not Found");
				list.add(statusMessageVO);
			}
			return new ResponseEntity<List<Object>>(list, HttpStatus.EXPECTATION_FAILED);
		}
		
	}
	
	@RequestMapping(value= "/getallhospitalData", method = RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> getAllhospitalData() {
		Map<String,Object> map=new HashMap<String,Object>();
		List<HospitalDataVO> hospitalDataVOlist=null;
		try {
			 hospitalDataVOlist = prepareListofHospitalDataVO(hospitalDataService.getallHospitalData());
			 map.put("hospitalData", hospitalDataVOlist);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch(Exception e ){
			map.put("status", "faild");
			if(e.getMessage()!=null)
			{
				map.put("message", e.getMessage());

			}else {
				map.put("message", "donor not found");
			}
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value= "/addhospital", method = RequestMethod.POST)
	public ResponseEntity<Map<String,Object>> addHospitalData(@RequestBody HospitalDataVO hospitalDatavo, UriComponentsBuilder builder) {
		HospitalData hospitalData = prepareModel(hospitalDatavo);
		Map<String,Object> map=new HashMap<String,Object>();
		String hospitalid ;
		try{
			hospitalid = String.valueOf(hospitalDataService.addHospitalData(hospitalData));
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(builder.path("/addhospital/{id}").buildAndExpand(hospitalData.getHospital_id()).toUri());
			map.put("headers", headers);
			map.put("status", "Hospitaladded");
			map.put("hospitalid", hospitalid);
			return new ResponseEntity<Map<String,Object>>(map, HttpStatus.CREATED);
			
		}catch (Exception e) {
			HttpHeaders headers = new HttpHeaders();
			headers.setLocation(builder.path("/addhospital/{id}").buildAndExpand(hospitalData.getHospital_id()).toUri());
			map.put("message", e.getMessage());
			map.put("headers", headers);
			map.put("status", "failed");
		return  new ResponseEntity<Map<String,Object>>(map, HttpStatus.OK);
			
		}
	
	}
	@RequestMapping(value="/updatehospital", method = RequestMethod.PUT )
	public ResponseEntity<Map<String,Object>> updatehospitalData(@RequestBody HospitalDataVO hospitalDataVO){
		    
		Map<String,Object> map=new HashMap<String,Object>();
			try {
				 HospitalData hospitalData = prepareModel(hospitalDataVO);	
				hospitalDataService.updateHospitalData(hospitalData);
				map.put("hospitalData", hospitalData);
				return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
			} catch(Exception e ){
				map.put("status", "faild");
				if(e.getMessage()!=null)
				{
					map.put("message", e.getMessage());

				}else {
					map.put("message", "donor not found");
				}
				return new ResponseEntity<Map<String,Object>>(map,HttpStatus.EXPECTATION_FAILED);
			}
	}
	
	@RequestMapping(value="/deletehospital/{id}", method = RequestMethod.DELETE )
	public ResponseEntity<Map<String,Object>> deleteApplicant(@PathVariable("id") Integer id) {
		hospitalDataService.deleteHospitalData(id);
		Map<String,Object> map=new HashMap<String,Object>();
		map.put("applicant", "deleted");
		return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
	}	

	private HospitalData prepareModel(HospitalDataVO hospitalDataVO){
		HospitalData hospitalData =new HospitalData();
		User user=new User();
		Hospitalbldunit hospitalbldunit =null;
		if(hospitalDataVO.getHospital_id() !=null) {
		hospitalData.setHospital_id(hospitalDataVO.getHospital_id());
		}
		hospitalData.setName(hospitalDataVO.getName());
		hospitalData.setAddress(hospitalDataVO.getAddress());
		hospitalData.setCity(hospitalDataVO.getCity());
		hospitalData.setCountry(hospitalDataVO.getCountry());
		hospitalData.setState(hospitalDataVO.getState());
		hospitalData.setLandLineNumber(hospitalDataVO.getLandLineNumber());
		hospitalData.setPinCode(hospitalDataVO.getPinCode());
		hospitalData.setuRL(hospitalDataVO.getuRL());
		hospitalData.setImage(hospitalDataVO.getImage());
		hospitalData.setpOC(hospitalDataVO.getpOC());
		
		if(hospitalDataVO.getUservo().getId() != null){
			user.setId(hospitalDataVO.getUservo().getId());
			}
			user.setUsername(hospitalDataVO.getUservo().getUsername());
			user.setPassword(hospitalDataVO.getUservo().getPassword());
			user.setUsertype(hospitalDataVO.getUservo().getUsertype());
			user.setEnabled(hospitalDataVO.getUservo().isEnabled());
			user.setGroup(hospitalDataVO.getUservo().getGroup());
			hospitalData.setUser(user);
			Set<Hospitalbldunit> hospitalbldunitlist=new HashSet<Hospitalbldunit>();
			for(HospitalbldunitVO hospitalbldunitVO : hospitalDataVO.getHospitalbldunitVO()) {
				hospitalbldunit =new Hospitalbldunit();
				if(hospitalbldunitVO.getId()!=null) {
					hospitalbldunit.setId(hospitalbldunitVO.getId());
				}
				hospitalbldunit.setBlood_group(hospitalbldunitVO.getBlood_group());
				hospitalbldunit.setUnit_count(hospitalbldunitVO.getUnit_count());
				hospitalbldunitlist.add(hospitalbldunit);
			}
			hospitalData.setHospitalbldunit(hospitalbldunitlist);
		return hospitalData;
	}

	private List<HospitalDataVO> prepareListofHospitalDataVO(List<HospitalData> hospitalDatas){
		List<HospitalDataVO> hospitalDatalist = null;
		if(hospitalDatas != null && !hospitalDatas.isEmpty()){
			hospitalDatalist = new ArrayList<HospitalDataVO>();
			HospitalDataVO hospitalDataVO = null;
			for(HospitalData hospitalData : hospitalDatas){
				hospitalDataVO = new HospitalDataVO();
				hospitalDataVO.setHospital_id(hospitalData.getHospital_id());
				hospitalDataVO.setName(hospitalData.getName());
				hospitalDataVO.setAddress(hospitalData.getAddress());
				hospitalDatalist.add(hospitalDataVO);
			}
		}
		return hospitalDatalist;
	}

	private HospitalDataUIVO prepareHospitalDatavo(HospitalData hospitalData){
		HospitalDataUIVO hospitalDatavo = new HospitalDataUIVO();
		UserVO uservo=new UserVO();
		hospitalDatavo.setHospital_id(hospitalData.getHospital_id());
		hospitalDatavo.setName(hospitalData.getName());
		hospitalDatavo.setAddress(hospitalData.getAddress());
		hospitalDatavo.setOrganization(hospitalData.getOrganization());
		hospitalDatavo.setCity(hospitalData.getCity());
		hospitalDatavo.setCountry(hospitalData.getCountry());
		hospitalDatavo.setState(hospitalData.getState());
		hospitalDatavo.setLandLineNumber(hospitalData.getLandLineNumber());
		hospitalDatavo.setPinCode(hospitalData.getPinCode());
		hospitalDatavo.setpOC(hospitalData.getpOC());
		hospitalDatavo.setImage(hospitalData.getImage());
		hospitalDatavo.setDirectDial(hospitalData.getDirectDial());
		hospitalDatavo.setuRL(hospitalData.getuRL());
		/*uservo.setId(hospitalData.getUser().getId());
		uservo.setUsername(hospitalData.getUser().getUsername());
		uservo.setPassword(hospitalData.getUser().getPassword());
		uservo.setGroup(hospitalData.getUser().getGroup());
		uservo.setUsertype(hospitalData.getUser().getUsertype());
		uservo.setEnabled(hospitalData.getUser().isEnabled());
		hospitalDatavo.setUservo(uservo);*/
		HospitalbldunitVO hospitalbldunitvo = null;
		Set<HospitalbldunitVO> hospitalbldunitVOlist=new HashSet<HospitalbldunitVO>();
		for(Hospitalbldunit hospitalbldunit:hospitalData.getHospitalbldunit()) {
			hospitalbldunitvo=new HospitalbldunitVO();
			hospitalbldunitvo.setId(hospitalbldunit.getId());
			hospitalbldunitvo.setBlood_group(hospitalbldunit.getBlood_group());
			hospitalbldunitvo.setUnit_count(hospitalbldunit.getUnit_count());
			hospitalbldunitVOlist.add(hospitalbldunitvo);
		}
		hospitalDatavo.setHospitalbldunitVO(hospitalbldunitVOlist);
		return hospitalDatavo;
	}

} 