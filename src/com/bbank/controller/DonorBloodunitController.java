package com.bbank.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bbank.model.DonerProfile;
import com.bbank.model.DonorBloodunit;
import com.bbank.model.HospitalData;
import com.bbank.model.Hosptlbldinvdtl;
import com.bbank.model.User;
import com.bbank.service.DonorBloodunitService;
import com.bbank.vo.DonerProfileVO;
import com.bbank.vo.DonorBloodunitVO;
import com.bbank.vo.HospitalDataVO;
import com.bbank.vo.UserVO;

@RestController
public class DonorBloodunitController {

	@Autowired
	private DonorBloodunitService donorBloodunitService;


	@RequestMapping(value="/getdonorbloodunit/{id}", method = RequestMethod.GET )
	public ResponseEntity<DonorBloodunitVO> getDonorBloodunitById(@PathVariable("id") Integer id) {
		DonorBloodunitVO donorBloodunitVO = preparModleVO(donorBloodunitService.findDonorBloodunitById(id));
		return new ResponseEntity<DonorBloodunitVO>(donorBloodunitVO, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/getalldonorBloodunit", method = RequestMethod.GET)
	public ResponseEntity<List<DonorBloodunitVO>> getAllDonorBloodunit() {
		List<DonorBloodunitVO> donorBloodunitVOs=prepareListofModle(donorBloodunitService.findAllDonorBloodunit());
		return new ResponseEntity<List<DonorBloodunitVO>>(donorBloodunitVOs, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/adddonorBloodunit", method = RequestMethod.POST)
	public ResponseEntity<Map<String,String>> addDonorBloodunit(@RequestBody DonorBloodunitVO donorBloodunitVO) {
		Map<String,String> map= new HashMap<String,String>();
		try{
		donorBloodunitService.saveDonorBloodunit(prepareModel(donorBloodunitVO));
		map.put("satus", "successfully");
		}catch (Exception e) {
			// TODO: handle exception
			map.put("errormessage", e.getMessage());
			return new ResponseEntity<Map<String,String>>(map, HttpStatus.CREATED);
		}
		return new ResponseEntity<Map<String,String>>(map, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/updatedonorBloodunit", method = RequestMethod.PUT )
	public ResponseEntity<Map<String,String>>  updateDonorBloodunit(@RequestBody DonorBloodunitVO donorBloodunitVO) {

		Map<String,String> map= new HashMap<String,String>();
		try{
		donorBloodunitService.updateDonorBloodunit(prepareModel(donorBloodunitVO));
		map.put("satus", "successfully");
		}catch (Exception e) {
			// TODO: handle exception
			map.put("errormessage", e.getMessage());
		}
		return new ResponseEntity<Map<String,String>>(map, HttpStatus.OK);
	}
	
	@RequestMapping(value="/donorBloodunit/{id}", method = RequestMethod.DELETE )
	public ResponseEntity<DonorBloodunitVO> deleteDonorBloodunit(@PathVariable("id") Integer id) {
		DonorBloodunitVO donorBloodunitVO = preparModleVO(donorBloodunitService.deleteDonorBloodunitById(id));
		return new ResponseEntity<DonorBloodunitVO>(donorBloodunitVO,HttpStatus.NO_CONTENT);
	}	

	private DonorBloodunit prepareModel(DonorBloodunitVO donorBloodunitVO){
		HospitalData hhospital=new HospitalData();
		Hosptlbldinvdtl hosptlbldinvdtl=new Hosptlbldinvdtl();
		DonorBloodunit donorBloodunit =new DonorBloodunit();
		donorBloodunit.setBloodid(donorBloodunitVO.getBloodid());
		donorBloodunit.setBlood_report(donorBloodunitVO.getBlood_report());
		donorBloodunit.setDonation_dt(donorBloodunitVO.getDonation_dt());
		donorBloodunit.setExpiry_dt(donorBloodunitVO.getExpiry_dt());
		donorBloodunit.setReimbursed(donorBloodunitVO.getReimbursed());
		//user.setId(donorBloodunitVO.getUservo().getId());
		//user.setUsername(donorBloodunitVO.getUservo().getUsername());
		//user.setUsertype(donorBloodunitVO.getUservo().getUsertype());
		//donorBloodunit.setUser(user);
		DonerProfile donerProfile =new DonerProfile();
			donerProfile.setDonorid(donorBloodunitVO.getDonerProfileVO().getDonorid());
			donerProfile.setBloodGroup(donorBloodunitVO.getDonerProfileVO().getBloodGroup());
			donorBloodunit.setDonerProfile(donerProfile);
		
			hhospital.setHospital_id(donorBloodunitVO.getHospitalDatavo().getHospital_id());
			donorBloodunit.setHospitalData(hhospital);
			hosptlbldinvdtl.setBlood_lot_number(donorBloodunitVO.getHosptlbldinvdtlVO().getBlood_lot_number());
			donerProfile.setDonorid(donorBloodunitVO.getDonerProfileVO().getDonorid());
			//hosptlbldinvdtl.getHospitalData().setHospital_id(donorBloodunitVO.getHospitalDatavo().getHospital_id());
			hosptlbldinvdtl.setDonerProfile(donerProfile);
			hosptlbldinvdtl.setHospitalData(hhospital);
			donorBloodunit.setHosptlbldinvdtl(hosptlbldinvdtl);
		return donorBloodunit;
	}

	private List<DonorBloodunitVO> prepareListofModle(List<DonorBloodunit> donorBloodunits){
		List<DonorBloodunitVO> DonorBloodunitVOs = null;
		if(donorBloodunits != null && !donorBloodunits.isEmpty()){
			DonorBloodunitVOs = new ArrayList<DonorBloodunitVO>();
			DonorBloodunitVO donorBloodunitVO = null;
			for(DonorBloodunit donorBloodunit : donorBloodunits){
				donorBloodunitVO = new DonorBloodunitVO();
				donorBloodunitVO.setBloodid(donorBloodunit.getBloodid());
				DonorBloodunitVOs.add(donorBloodunitVO);
			}
		}
		return DonorBloodunitVOs;
	}

	private DonorBloodunitVO preparModleVO(DonorBloodunit donorBloodunit){
		DonorBloodunitVO donorBloodunitVO = new DonorBloodunitVO();
		UserVO uservo=new UserVO();
		HospitalDataVO host=new HospitalDataVO();
		donorBloodunitVO.setBloodid(donorBloodunit.getBloodid());
		donorBloodunitVO.setBlood_report(donorBloodunit.getBlood_report());
		donorBloodunitVO.setReimbursed(donorBloodunit.getReimbursed());
		//uservo.setId(donorBloodunit.getUser().getId());
		//donorBloodunitVO.setUservo(uservo);
		
		return donorBloodunitVO;
	}
}

