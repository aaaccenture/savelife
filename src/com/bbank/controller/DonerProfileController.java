package com.bbank.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.bbank.model.DonerProfile;
import com.bbank.model.DonorBloodunit;
import com.bbank.model.User;
import com.bbank.service.DonerProfileService;
import com.bbank.vo.BankVO;
import com.bbank.vo.DonerProfileVO;
import com.bbank.vo.DonorBloodunitVO;
import com.bbank.vo.UserVO;

@Controller
public class DonerProfileController {

	@Autowired
	private DonerProfileService donerProfileService;

	@RequestMapping(value = "/donorprofile", method = RequestMethod.POST)
	public ResponseEntity<Map<String,String>> saveDonerProfile(@RequestBody DonerProfileVO donerprofilevo){	
		Map<String,String> map=new HashMap<String,String>();
		String donorid;
		try {
			//donorid=donerProfileService.saveDonerProfile(prepareModel(donerprofilevo));
			donorid = String.valueOf(donerProfileService.saveDonerProfile(prepareModel(donerprofilevo)));
			map.put("status", "success");
			map.put("message", "added");
			map.put("donorid", donorid)	;		
			return new ResponseEntity<Map<String,String>>(map,HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("status", "faild");
			map.put("message", e.getMessage());
			return new ResponseEntity<Map<String,String>>(map,HttpStatus.BAD_REQUEST);
		}	

	}
	@RequestMapping(value = "/getdonerbyid/{id}", method = RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> findByDonerProfileId(@PathVariable int id){	
		Map<String,Object> map=new HashMap<String,Object>();
		try {
			DonerProfileVO donerprofilevo=prepareDonerProfileVO(donerProfileService.findByDonerProfileId(id));
			map.put("donorprofile", donerprofilevo);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch(Exception e ){
			map.put("status", "faild");
			if(e.getMessage()!=null)
			{
				map.put("message", e.getMessage());

			}else {
				map.put("message", "donor not found");
			}
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.EXPECTATION_FAILED);
		}


	}

	@RequestMapping(value = "/getalldoner", method = RequestMethod.GET)
	public ResponseEntity<List<DonerProfileVO>> findByAllDonerProfile(){		
		List<DonerProfileVO> donerprofilevo=prepareListofDonerProfileVO(donerProfileService.findAllDonerProfile());
		return new ResponseEntity<List<DonerProfileVO>>(donerprofilevo,HttpStatus.OK);

	}

	@RequestMapping(value = "/deletedonerprofile/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Map<String,String>> deleteByDonerProfileid(@PathVariable int id){		
		donerProfileService.deleteByDonerProfileid(id);
		Map<String,String> map=new HashMap<>();
		map.put("status", "userdeleted");
		return new ResponseEntity<Map<String,String>>(map,HttpStatus.OK);

	}

	@RequestMapping(value = "/updatedonerprofile", method = RequestMethod.PUT)
	public ResponseEntity<Map<String,Object>> updateDonerProfile(@RequestBody DonerProfileVO donerprofilevo){	
		Map<String,Object> map=new HashMap<String,Object>();
		try {
			donerProfileService.updateDonerProfile(prepareModel(donerprofilevo));
			map.put("updateduser", donerprofilevo);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("user", "user not found"+donerprofilevo.getFirstName());
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		}

	}



	private DonerProfileVO prepareDonerProfileVO(DonerProfile donerprofile){
		DonerProfileVO donerprofilevo = new DonerProfileVO();
		//UserVO uservo=new UserVO();
		donerprofilevo.setDonorid(donerprofile.getDonorid());
		donerprofilevo.setFirstName(donerprofile.getFirstName());
		donerprofilevo.setLastname(donerprofile.getLastname());
		donerprofilevo.setMiddle_name(donerprofile.getMiddle_name());
		donerprofilevo.setFatherName(donerprofile.getFatherName());
		donerprofilevo.setBloodGroup(donerprofile.getBloodGroup());
		donerprofilevo.setAddress(donerprofile.getAddress());
		donerprofilevo.setCountry(donerprofile.getCountry());
		donerprofilevo.setCity(donerprofile.getCity());
		donerprofilevo.setPinCode(donerprofile.getPinCode());;
		donerprofilevo.setProof_id(donerprofile.getProof_id());
		donerprofilevo.setProofId_typ(donerprofile.getProofId_typ());
		donerprofilevo.setdOB(donerprofile.getdOB());
		donerprofilevo.setImage(null);
		donerprofilevo.setState(donerprofile.getState());
		donerprofilevo.setEmail(donerprofile.getEmail());
		DonorBloodunitVO donorBloodunitVO = null;
		List<DonorBloodunitVO> donorunitvolist=new ArrayList<DonorBloodunitVO>();
		for(DonorBloodunit donorBloodunit:donerprofile.getDonorBloodunit()) {
			donorBloodunitVO=new DonorBloodunitVO();
			donorBloodunitVO.setBloodid(donorBloodunit.getBloodid());
			donorBloodunitVO.setBlood_report(donorBloodunit.getBlood_report());
			donorBloodunitVO.setDonation_dt(donorBloodunit.getDonation_dt());
			donorBloodunitVO.setReimbursed(donorBloodunit.getReimbursed());
			donorunitvolist.add(donorBloodunitVO);
		}
		donerprofilevo.setDonorBloodunitVO(donorunitvolist);
		/*uservo.setId(donerprofile.getUser().getId());
		uservo.setUsername(donerprofile.getUser().getUsername());
		uservo.setPassword(donerprofile.getUser().getPassword());
		uservo.setEnabled(donerprofile.getUser().isEnabled());
		uservo.setUsertype(donerprofile.getUser().getUsertype());
		uservo.setGroup(donerprofile.getUser().getGroup());*/
		//donerprofilevo.setUservo(uservo);

		return donerprofilevo;
	}

	private DonerProfile prepareModel(DonerProfileVO donerprofilevo){
		DonerProfile donerprofile =new DonerProfile();
		User user=new User();
		donerprofile.setDonorid(donerprofilevo.getDonorid());
		donerprofile.setFirstName(donerprofilevo.getFirstName());
		donerprofile.setLastname(donerprofilevo.getLastname());
		donerprofile.setMiddle_name(donerprofilevo.getMiddle_name());
		donerprofile.setFatherName(donerprofilevo.getFatherName());
		donerprofile.setBloodGroup(donerprofilevo.getBloodGroup());
		donerprofile.setAddress(donerprofilevo.getAddress());
		donerprofile.setCountry(donerprofilevo.getCountry());
		donerprofile.setCity(donerprofilevo.getCity());
		donerprofile.setPinCode(donerprofilevo.getPinCode());
		donerprofile.setProof_id(donerprofilevo.getProof_id());
		donerprofile.setProofId_typ(donerprofilevo.getProofId_typ());
		donerprofile.setBloodGroup(donerprofilevo.getBloodGroup());
		donerprofile.setEmail(donerprofilevo.getEmail());
		donerprofile.setMobile(donerprofilevo.getMobile());
		donerprofile.setState(donerprofilevo.getState());
		donerprofile.setdOB(donerprofilevo.getdOB());
		donerprofile.setImage(null);
		if(donerprofilevo.getUservo().getId() != null){
			user.setId(donerprofilevo.getUservo().getId());
		}
		user.setUsername(donerprofilevo.getUservo().getUsername());
		user.setPassword(donerprofilevo.getUservo().getPassword());
		user.setUsertype(donerprofilevo.getUservo().getUsertype());
		user.setGroup(donerprofilevo.getUservo().getGroup());
		donerprofile.setUser(user);			

		return donerprofile;
	}

	private List<DonerProfileVO> prepareListofDonerProfileVO(List<DonerProfile> donerprofiles){
		List<DonerProfileVO> donerprofilevos = null;
		if(donerprofiles != null && !donerprofiles.isEmpty()){
			donerprofilevos = new ArrayList<DonerProfileVO>();
			DonerProfileVO donerprofilevo = null;
			UserVO uservo=null;
			for(DonerProfile donerprofile : donerprofiles){
				donerprofilevo = new DonerProfileVO();
				uservo=new UserVO();
				donerprofilevo.setDonorid(donerprofile.getDonorid());
				donerprofilevo.setFirstName(donerprofile.getFirstName());
				donerprofilevo.setLastname(donerprofile.getLastname());
				donerprofilevo.setMiddle_name(donerprofile.getMiddle_name());
				donerprofilevo.setFatherName(donerprofile.getFatherName());
				donerprofilevo.setBloodGroup(donerprofile.getBloodGroup());
				donerprofilevo.setAddress(donerprofile.getAddress());
				donerprofilevo.setCountry(donerprofile.getCountry());
				donerprofilevo.setCity(donerprofile.getCity());
				donerprofilevo.setPinCode(donerprofile.getPinCode());
				donerprofilevo.setProof_id(donerprofile.getProof_id());
				donerprofilevo.setdOB(donerprofile.getdOB());
				donerprofilevo.setProofId_typ(donerprofile.getProofId_typ());
				donerprofilevo.setImage(null);uservo.setId(donerprofile.getUser().getId());
				uservo.setUsername(donerprofile.getUser().getUsername());
				uservo.setPassword(donerprofile.getUser().getPassword());
				uservo.setEnabled(donerprofile.getUser().isEnabled());
				uservo.setUsertype(donerprofile.getUser().getUsertype());
				uservo.setGroup(donerprofile.getUser().getGroup());
				donerprofilevo.setUservo(uservo);
				donerprofilevos.add(donerprofilevo);
			}
		}
		return donerprofilevos;
	}

}
