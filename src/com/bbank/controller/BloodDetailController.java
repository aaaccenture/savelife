package com.bbank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.bbank.model.BloodDetail;
import com.bbank.service.BloodetailService;
import com.bbank.vo.BloodDetailVO;

@RestController
public class BloodDetailController {

	@Autowired
	private BloodetailService bloodetailService;


	@RequestMapping(value="/getblooddetail/{id}", method = RequestMethod.GET )
	public ResponseEntity<BloodDetailVO> getBloodDetailVOById(@PathVariable("id") Integer id) {
		BloodDetailVO bloodDetailVO = preparModleVO(bloodetailService.findBloodDetailById(id));
		return new ResponseEntity<BloodDetailVO>(bloodDetailVO, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/getallblooddetail", method = RequestMethod.GET)
	public ResponseEntity<List<BloodDetailVO>> getAllBloodDetail() {
		List<BloodDetailVO> bloodDetailVOs=prepareListofModle(bloodetailService.findAllBloodDetail());
		return new ResponseEntity<List<BloodDetailVO>>(bloodDetailVOs, HttpStatus.OK);
	}
	
	@RequestMapping(value= "/addblooddetail", method = RequestMethod.POST)
	public ResponseEntity<BloodDetailVO> addBloodDetail(@RequestBody BloodDetailVO bloodDetailvo) {

		return new ResponseEntity<BloodDetailVO>(bloodDetailvo, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/updateblooddetail/{id}", method = RequestMethod.PUT )
	public ResponseEntity<BloodDetailVO> updateblooddetail(@RequestBody BloodDetailVO bloodDetailvo) {

		return new ResponseEntity<BloodDetailVO>(bloodDetailvo, HttpStatus.OK);
	}
	
	@RequestMapping(value="/blooddetail/{id}", method = RequestMethod.DELETE )
	public ResponseEntity<BloodDetailVO> deleteblooddetail(@PathVariable("id") Integer id) {

		BloodDetailVO bloodDetailVO = preparModleVO(bloodetailService.deleteBloodDetailById(id));
		return new ResponseEntity<BloodDetailVO>(bloodDetailVO,HttpStatus.NO_CONTENT);
	}	

	private BloodDetail prepareModel(BloodDetailVO bloodDetailVO){
		BloodDetail bloodDetail =new BloodDetail();


		return bloodDetail;
	}

	private List<BloodDetailVO> prepareListofModle(List<BloodDetail> bloodDetails){
		List<BloodDetailVO> bloodDetailVOs = null;
		if(bloodDetails != null && !bloodDetails.isEmpty()){
			bloodDetailVOs = new ArrayList<BloodDetailVO>();
			BloodDetailVO bloodDetailVO = null;
			for(BloodDetail bloodDetail : bloodDetails){
				bloodDetailVO = new BloodDetailVO();
				bloodDetailVO.setBlooddtlid(bloodDetail.getBlooddtlid());
				bloodDetailVO.sethB(bloodDetail.gethB());
				bloodDetailVO.setLymphocytes(bloodDetail.getLymphocytes());
				bloodDetailVOs.add(bloodDetailVO);
			}
		}
		return bloodDetailVOs;
	}

	private BloodDetailVO preparModleVO(BloodDetail bloodDetail){
		BloodDetailVO bloodDetailVO = new BloodDetailVO();
		bloodDetailVO.setBlooddtlid(bloodDetail.getBlooddtlid());
		bloodDetailVO.sethB(bloodDetail.gethB());
		bloodDetailVO.setLymphocytes(bloodDetail.getLymphocytes());
		return bloodDetailVO;
	}
}

