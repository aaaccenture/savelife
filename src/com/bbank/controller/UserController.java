package com.bbank.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.bbank.model.DonerProfile;
import com.bbank.model.User;
import com.bbank.model.UserRole;
import com.bbank.service.DonorBloodunitService;
import com.bbank.service.UserService;
import com.bbank.vo.DonerProfileVO;
import com.bbank.vo.UserUIVO;
import com.bbank.vo.UserVO;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private DonorBloodunitService onorBloodunitService;
	
	@RequestMapping(value = { "/"}, method = RequestMethod.GET)
	public ModelAndView defaultPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("home");
		return model;

	}
	
	@RequestMapping(value = { "/loginpage"}, method = RequestMethod.GET)
	public ModelAndView defaultloginPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("index");
		return model;

	}

	@RequestMapping(value = "/loginstatus", method = RequestMethod.GET)
	public ResponseEntity<Object> loginstatus(){

		Map<String,Object> map=new HashMap<>();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
			map.put("username", userDetail.getUsername());
			map.put("authorities", userDetail.getAuthorities());
		}
		return new ResponseEntity<Object>(map,HttpStatus.OK);

	}

	@RequestMapping(value = "/adduser", method = RequestMethod.POST)
	public ResponseEntity<String> adduser(@RequestBody User user){		
		userService.saveUser(user);
		return new ResponseEntity<String>("user added",HttpStatus.OK);

	}
	
	@RequestMapping(value = "/deleteuser/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Map<String,String>> deleteByUserid(@PathVariable int id){		
		userService.deleteByUserid(id);
		Map<String,String> map=new HashMap<>();
		map.put("status", "userdeleted");
		return new ResponseEntity<Map<String,String>>(map,HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/updateuser", method = RequestMethod.PUT)
	public ResponseEntity<Map<String,Object>> updateUserByName(@RequestBody User user){	
		Map<String,Object> map=new HashMap<String,Object>();
		try {
			User updateduser=userService.updateUserByName(user);
			map.put("updateduser", updateduser);
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			map.put("user", "user not found"+user.getUsername());
			return new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
		}
			
	}
	
	@RequestMapping(value = "/getuser/{id}", method = RequestMethod.GET)
	public ResponseEntity<UserVO> getUser(@PathVariable int id){		
		User user= userService.findById(id);
		UserVO uservo = new UserVO();
		uservo.setId(user.getId());
		uservo.setUsername(user.getUsername());
		uservo.setUsertype(user.getUsertype());
		uservo.setPassword(user.getPassword());
		uservo.setEnabled(user.isEnabled());
		return new ResponseEntity<UserVO>(uservo,HttpStatus.OK);

	}
		
	@RequestMapping(value = "/getuserbyname/{username}", method = RequestMethod.GET)
	public ResponseEntity<UserVO> getUserByUserName(@PathVariable String username){		
		User user= userService.findByUserName(username);
		UserVO uservo = new UserVO();
		
		uservo.setId(user.getId());
		uservo.setUsername(user.getUsername());
		uservo.setUsertype(user.getUsertype());
		uservo.setPassword(user.getPassword());
		uservo.setEnabled(user.isEnabled());
		return new ResponseEntity<UserVO>(uservo,HttpStatus.OK);

	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> login(@RequestParam(value = "usertype", required = false) String usertype ,@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request,HttpServletResponse respo) {

		Map<String,Object> model = new HashMap<>();
		try{
		if (error != null) {
			model.put("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
			return new ResponseEntity<Map<String,Object>>(model,HttpStatus.UNAUTHORIZED);
		}

		
		if (logout != null) {	
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null){    
				new SecurityContextLogoutHandler().logout(request, respo, auth);
				//persistentTokenBasedRememberMeServices.logout(request, response, auth);
				SecurityContextHolder.getContext().setAuthentication(null);
				model.put("status","logout");		
			}
			return new ResponseEntity<Map<String,Object>>(model,HttpStatus.OK);
			
		}else{
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			UserDetails userDetail=null;
			if (auth != null && ! auth.getPrincipal().equals("anonymousUser")){ 
			userDetail = (UserDetails) auth.getPrincipal();
			model.put("status","logddin");
			}
			//String usertype="admin";
			User userdetail=userService.findByUserName(userDetail.getUsername());
			UserUIVO doervo= new UserUIVO();
			if(userdetail.getGroup().equals("Donor")) {
			doervo.setDonoid(userdetail.getDonerProfile().getDonorid());
			doervo.setName(userdetail.getDonerProfile().getFirstName());
			doervo.setGroupame(userdetail.getGroup());
			}else if(userdetail.getGroup().equals("Hospital")){
				doervo.setHospitlid(userdetail.getHospitalData().getHospital_id());
				doervo.setName(userdetail.getHospitalData().getName());
				doervo.setGroupame(userdetail.getGroup());
			}
			
			//doervo.setLastname(userdetail.getDonerProfile().getLastname());
			//UserVO uservo = new UserVO();
			//uservo.setDonerProfilevo(doervo);
			doervo.setUid(userdetail.getId());
			doervo.setUsername(userdetail.getUsername());
			//uservo.setPassword(userdetail.getPassword());
			doervo.setUserType(userdetail.getUsertype());
			//uservo.setEnabled(userdetail.isEnabled());
			model.put("UserDetail",doervo);
			model.put("AccessToken", "bb1234");
			return new ResponseEntity<Map<String,Object>>(model,HttpStatus.OK);
		}
		}catch (Exception e) {
			// TODO: handle exception
			if(e.getMessage()==null){
				model.put("status","userType not match with user");
				return new ResponseEntity<Map<String,Object>>(model,HttpStatus.BAD_REQUEST);
			}
			model.put("status",e.getMessage());
			return new ResponseEntity<Map<String,Object>>(model,HttpStatus.BAD_REQUEST);
		}
					
		}
	
	


	// customize the error message
	private String getErrorMessage(HttpServletRequest request, String key) {

		Exception exception = (Exception) request.getSession().getAttribute(key);

		String error = "";
		if (exception instanceof BadCredentialsException) {
			error = "Invalid username and password!";
		} else if (exception instanceof LockedException) {
			error = exception.getMessage();
		} else {
			error = "Invalid username and password!";
		}

		return error;

	}

	// for 403 access denied page
	@RequestMapping(value = "/403")
	public ResponseEntity<Object> accesssDenied() {
		Map<String,Object> model = new HashMap<>();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
			model.put("username", userDetail.getUsername());
			model.put("message","Hi " + userDetail.getUsername()
			+ ", you do not have permission to access this page!");

		}	
		return new ResponseEntity<Object>(model,HttpStatus.UNAUTHORIZED);

	}



}