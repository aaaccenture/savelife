package com.bbank.dao;

import java.util.List;

import com.bbank.model.User;


public interface UserDao {

	User findById(int id);
	
	User findByUserName(String username);
	
	User findRMByTypeId(String typeid) throws Exception;
	
    User findFAByTypeId(String typeid) throws Exception;
	
	void save(User user);
	
	void deleteByUserid(int id);
	
	List<User> findAllUsers();
	
	User updateUserByName(User user) throws Exception;
	
	User findUserNameType(String  type, String username);
	
	public void updateUser(User user);

}

