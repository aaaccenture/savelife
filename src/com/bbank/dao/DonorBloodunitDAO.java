package com.bbank.dao;

import java.util.List;

import com.bbank.model.DonorBloodunit;

public interface DonorBloodunitDAO {
	
	void saveDonorBloodunit(DonorBloodunit donorBloodunit) throws Exception;

	void updateDonorBloodunit(DonorBloodunit donorBloodunit);

	DonorBloodunit findDonorBloodunitById(int id);

	List<DonorBloodunit> findAllDonorBloodunit();

	DonorBloodunit deleteDonorBloodunitById(int id);

}
