package com.bbank.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbank.model.User;


@Transactional
@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	static final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	public User findById(int id) {
		User user = getByKey(id);
		return user;
	}

	public User findByUserName(String username) {
		logger.info("username : {}", username);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		User user = (User)crit.uniqueResult();
		return user;
	}
	
	public User findFAByTypeId(String typeid) throws Exception {
		logger.info("username : {}", typeid);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("typeid", typeid));
		crit.add(Restrictions.eq("type", "FA"));
		User user = (User)crit.uniqueResult();
		return user;
	}

	public User findRMByTypeId(String typeid) throws Exception {
		logger.info("username : {}", typeid);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("typeid", typeid));
		crit.add(Restrictions.eq("type", "RM"));
		User user = (User)crit.uniqueResult();
		return user;
	}


	@SuppressWarnings("unchecked")
	public List<User> findAllUsers() {
		String hql = "FROM User as users ORDER BY users.id";
		List<User>	users = sessionFactory.getCurrentSession().createQuery(hql).list();
		return users;
	}

	public void save(User user) {
		//persist(user);
		sessionFactory.getCurrentSession().save (user);
	}

	public void deleteByUserid(int id) {
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		User user = (User)crit.uniqueResult();
		delete(user);
	}

	@Override
	public User updateUserByName(User user) throws Exception {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().saveOrUpdate(user);
		return user;
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(user);
	}

	@Override
	public User findUserNameType(String type, String username) {
		logger.info("username : {}", username);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("username", username));
		crit.add(Restrictions.eq("usertype", type));
		User user = (User)crit.uniqueResult();
		return user;
	}

}
