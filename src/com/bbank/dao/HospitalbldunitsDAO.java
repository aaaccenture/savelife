package com.bbank.dao;

import com.bbank.model.Hospitalbldunit;

public interface HospitalbldunitsDAO {
	
   void  adddonorbldunit(Hospitalbldunit hospitalbldunit ) throws Exception;
	
	Hospitalbldunit gethospitalbldunitby(int hid,String bloodgroup);

}
