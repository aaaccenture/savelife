package com.bbank.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbank.model.DonerProfile;
import com.bbank.model.HospitalData;
import com.bbank.model.Hospitalbldunit;

@Transactional
@Repository("hospitalbldunitsDAO")
public class HospitalbldunitsDAOImpl extends AbstractDao<Integer, Hospitalbldunit> implements HospitalbldunitsDAO{

	static final Logger logger = LoggerFactory.getLogger(HospitalbldunitsDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HospitalDataDAO hospitalDataDAO;
	
	@Override
	public void adddonorbldunit(Hospitalbldunit hospitalbldunit) throws Exception {
		Session session = null;
		Transaction tx = null;
		int id;
		try{	
			session=sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(hospitalbldunit);
			tx.commit();
			tx=null;
		}catch(Exception e){
			System.out.println("Exception occured. "+e.getMessage());
			tx.rollback();
			throw new Exception(e.getMessage());
		}finally{
			System.out.println("Closing session");
			session.close();
		}
	
	}

	@Override
	public Hospitalbldunit gethospitalbldunitby(int hospital_id, String blood_group) {
		Session session=sessionFactory.openSession();
		String hql = "FROM Hospitalbldunit WHERE hospital_id= :hospital_id and blood_group= :blood_group";
		Query query = session.createQuery(hql);
		query.setParameter("hospital_id",hospital_id);
		query.setParameter("blood_group",blood_group);
		List results = query.list();
		Hospitalbldunit hospitalbldunit=(Hospitalbldunit) results.get(0);
		session.close();
		return hospitalbldunit;
	}

}
