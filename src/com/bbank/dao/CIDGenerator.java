package com.bbank.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class CIDGenerator{

	@Autowired
	private SessionFactory sessionFactory;
	
	public int generateid()
			throws HibernateException {
		int nextCid = 0;
		Transaction tx=null;
		try{
			
			Session session=sessionFactory.openSession();
			tx=session.beginTransaction();
			String hql1="from LendarDiscovery c";
			Query query=session.createQuery(hql1);
			List l1=query.list();
			if(l1.size()==0){
				nextCid=100;
				System.out.println(nextCid);
			}
			else{
				System.out.println("else...");
				String hql2="select max(id) from LendarDiscovery c";
				query=session.createQuery(hql2);
				String ccid=query.list().get(0).toString();
				int cid=Integer.parseInt(ccid);
				nextCid=cid+1;
				
			}
			
			tx.commit();
			
			session.close();
		}catch (Exception e) {
			if(tx!=null){
				tx.rollback();
			}
			
			e.printStackTrace();
		}
		
		System.out.println(nextCid);
		return nextCid;
	}


	}
	
