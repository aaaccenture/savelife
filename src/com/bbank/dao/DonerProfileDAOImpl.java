package com.bbank.dao;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.bbank.model.DonerProfile;
import com.bbank.model.User;

@Transactional
@Repository("donerProfileDAO")
public class DonerProfileDAOImpl extends AbstractDao<Integer, DonerProfile> implements DonerProfileDAO{

	static final Logger logger = LoggerFactory.getLogger(DonerProfileDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public int saveDonerProfile(DonerProfile donerProfile) throws Exception{
		Session session = null;
		Transaction tx = null;
		int id;
		try{	
			session=sessionFactory.openSession();
			tx = session.beginTransaction();
			id=(int) session.save(donerProfile);
			tx.commit();
			tx=null;
		}catch(Exception e){
			System.out.println("Exception occured. "+e.getMessage());
			tx.rollback();
			throw new Exception(e.getMessage());
		}finally{
			System.out.println("Closing session");
			session.close();
		}
       return id;
	}

	@Override
	public void updateDonerProfile(DonerProfile donerProfile) {
		// TODO Auto-generated method stub
		Session session = null;
		Transaction tx = null;
		try{	
			session=sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(donerProfile);
			tx.commit();
			tx=null;
		}catch(Exception e){
			System.out.println("Exception occured. "+e.getMessage());
			tx.rollback();
			e.printStackTrace();
		}finally{
			System.out.println("Closing session");
			session.close();
		}
		
	}

	@Override
	public DonerProfile findByDonerProfileId(int id) throws Exception {
		// TODO Auto-generated method stub

		/*DonerProfile donerProfile = (DonerProfile) sessionFactory.getCurrentSession().get(DonerProfile.class, id);
		if(donerProfile!=null){
			Hibernate.initialize(donerProfile.getUser());
		}*/

		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("donorid", id));
		DonerProfile donerProfile = (DonerProfile)crit.uniqueResult();
		if(donerProfile!=null){
			Hibernate.initialize(donerProfile.getUser());
			Hibernate.initialize(donerProfile.getDonorBloodunit());
		}
		//DonerProfile donerProfile1=getByKey(id);
		return donerProfile;
	}

	@Override
	public List<DonerProfile> findAllDonerProfile() {
		// TODO Auto-generated method stub
		String hql = "FROM DonerProfile as donerProfile ORDER BY donerProfile.donorid";
		@SuppressWarnings("unchecked")
		List<DonerProfile>	donerProfiles = sessionFactory.getCurrentSession().createQuery(hql).list();
		return donerProfiles;
	}

	@Override
	public void deleteByDonerProfileid(int id) {
		// TODO Auto-generated method stub

		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("id", id));
		DonerProfile donerProfile = (DonerProfile)crit.uniqueResult();
		delete(donerProfile);

	}

}
