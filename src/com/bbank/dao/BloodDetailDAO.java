package com.bbank.dao;

import java.util.List;

import com.bbank.model.BloodDetail;

public interface BloodDetailDAO {

	
	void saveBloodDetail(BloodDetail bloodDetail) throws Exception;

	void updateBloodDetail(BloodDetail bloodDetail);

	BloodDetail findBloodDetailById(int id);

	List<BloodDetail> findAllBloodDetail();

	BloodDetail deleteBloodDetailById(int id);
}
