package com.bbank.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbank.model.BloodDetail;
import com.bbank.model.User;
import com.bbank.model.UserRole;

@Transactional
@Repository("bloodDetailDAO")
public class BloodDetailDAOImpl extends AbstractDao<BloodDetail, Integer> implements BloodDetailDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void saveBloodDetail(BloodDetail bloodDetail) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBloodDetail(BloodDetail bloodDetail) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BloodDetail findBloodDetailById(int id) {
		// TODO Auto-generated method stub
		BloodDetail bloodDetail=(com.bbank.model.BloodDetail) sessionFactory.getCurrentSession().get(BloodDetail.class, id);
		return bloodDetail;
	}

	@Override
	public List<BloodDetail> findAllBloodDetail() {
		// TODO Auto-generated method stub
		String hql = "FROM BloodDetail as bloodDetail ORDER BY bloodDetail.blooddtlid";
		List<BloodDetail>	bloodDetails = sessionFactory.getCurrentSession().createQuery(hql).list();

		return bloodDetails;
	}

	@Override
	public BloodDetail deleteBloodDetailById(int id) {
		return null;
		// TODO Auto-generated method stub
		
	}
	
	
}
