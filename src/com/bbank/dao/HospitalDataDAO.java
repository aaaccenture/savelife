package com.bbank.dao;
import java.io.Serializable;
import java.util.List;


import com.bbank.model.HospitalData;


public interface HospitalDataDAO {
    List<HospitalData> getAllHospitalData();
    HospitalData getHospitalDataById(int hid) throws Exception;
    int addHospitalData(HospitalData hospitalData) throws Exception;
    void updateHospitalData(HospitalData hospitalData);
    void deleteHospitalData(int hid);
        
}
 