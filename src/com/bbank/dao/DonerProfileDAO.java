package com.bbank.dao;

import java.util.List;

import com.bbank.model.DonerProfile;

public interface DonerProfileDAO {

	
	int saveDonerProfile(DonerProfile donerProfile) throws Exception;

	void updateDonerProfile(DonerProfile donerProfile);

	DonerProfile findByDonerProfileId(int id) throws Exception;

	List<DonerProfile> findAllDonerProfile();

	void deleteByDonerProfileid(int id);
}
