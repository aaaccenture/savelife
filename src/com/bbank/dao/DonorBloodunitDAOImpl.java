package com.bbank.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbank.model.DonerProfile;
import com.bbank.model.DonorBloodunit;
import com.bbank.model.HospitalData;
import com.bbank.model.Hospitalbldunit;
import com.bbank.model.Hosptlbldinvdtl;
import com.bbank.model.User;

@Transactional
@Repository("donorBloodunitDAO")
public class DonorBloodunitDAOImpl extends AbstractDao<Integer,DonorBloodunit> implements DonorBloodunitDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private HospitalbldunitsDAO hospitalbldunitsDAO;
	
	@Autowired
	private HospitalDataDAO hospitalDataDAO;
	

	@Override
	public void saveDonorBloodunit(DonorBloodunit donorBloodunit) throws Exception {
		Session session = null;
		Transaction tx = null;
		int id;
		//Hosptlbldinvdtl hosptlbldinvdtl =new Hosptlbldinvdtl();
		try{	
			session=sessionFactory.openSession();
			tx = session.beginTransaction();
			donorBloodunit.getHosptlbldinvdtl().setBlood_group(donorBloodunit.getDonerProfile().getBloodGroup());
			//hosptlbldinvdtl.setBlood_lot_number(donorBloodunit.getHosptlbldinvdtl().getBlood_lot_number());
			//hosptlbldinvdtl.getDonerProfile().setDonorid(donorBloodunit.getDonerProfile().getDonorid());
			//hosptlbldinvdtl.getHospitalData().setHospital_id(donorBloodunit.getHospitalData().getHospital_id());
			id=(int) session.save(donorBloodunit);
			donorBloodunit.getHosptlbldinvdtl().setBloodinvid(id);
			//session.merge(donorBloodunit.getHosptlbldinvdtl());
			Hospitalbldunit hospitalbldunit=hospitalbldunitsDAO.gethospitalbldunitby(donorBloodunit.getHospitalData().getHospital_id(), donorBloodunit.getDonerProfile().getBloodGroup());
			HospitalData hospital=hospitalDataDAO.getHospitalDataById(donorBloodunit.getHospitalData().getHospital_id());
			int count=hospitalbldunit.getUnit_count()+1;
			hospitalbldunit.setUnit_count(count);
			if(donorBloodunit.getHospitalData().getHospital_id() !=null) {
				//User user=new User();
				//user.setId(hospital.getUser().getId());
				//hospitalbldunit.getHospitalData().setUser(user);
				Hospitalbldunit hospitalbldunitnew =new Hospitalbldunit();
				hospitalbldunitnew.setHospitalData(hospital);
				hospitalbldunitnew.setUnit_count(count);
				hospitalbldunitnew.setId(hospitalbldunit.getId());
				hospitalbldunitnew.setBlood_group(hospitalbldunit.getBlood_group());
			    session.merge(hospitalbldunitnew);
		   }else {
			   session.saveOrUpdate(hospitalbldunit);
		   }
			tx.commit();
			tx=null;
		}catch(Exception e){
			System.out.println("Exception occured. "+e.getMessage());
			tx.rollback();
			throw new Exception(e.getMessage());
		}finally{
			System.out.println("Closing session");
			session.close();
		}
      
	}

	@Override
	public void updateDonorBloodunit(DonorBloodunit donorBloodunit) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public DonorBloodunit findDonorBloodunitById(int id) {
		DonorBloodunit donorBloodunit = getByKey(id);
		return donorBloodunit;
	}

	
	@Override
	public List<DonorBloodunit> findAllDonorBloodunit() {
		// TODO Auto-generated method stub
		String hql = "FROM DonorBloodunit as donorBloodunit ORDER BY donorBloodunit.Bloodid";
		@SuppressWarnings("unchecked")
		List<DonorBloodunit> donorBloodunits = sessionFactory.getCurrentSession().createQuery(hql).list();
		return donorBloodunits;
	}

	@Override
	public DonorBloodunit deleteDonorBloodunitById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
