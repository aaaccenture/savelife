package com.bbank.dao;
import java.io.Serializable;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bbank.model.DonerProfile;
import com.bbank.model.HospitalData;
import com.bbank.model.Hospitalbldunit;


@Transactional
@Repository
public class HospitalDataDAOImpl extends AbstractDao<Integer, HospitalData> implements HospitalDataDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<HospitalData> getAllHospitalData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HospitalData getHospitalDataById(int hid) throws Exception{
		// TODO Auto-generated method stub
		//HospitalData hospitalData=(HospitalData) sessionFactory.getCurrentSession().get(HospitalData.class, hid);
		Criteria crit = createEntityCriteria();
		crit.add(Restrictions.eq("hospital_id", hid));
		HospitalData hospitalData = (HospitalData) crit.uniqueResult();
		if(hospitalData!=null){
			Hibernate.initialize(hospitalData.getUser());
			Hibernate.initialize(hospitalData.getHospitalbldunit());
		}
		return hospitalData;
	}

	@Override
	public int addHospitalData(HospitalData hospitalData) throws Exception {
		// TODO Auto-generated method stub
			Session session = null;
			Transaction tx = null;
			HospitalData hospitalDatanew=new HospitalData();
			int id;
			try{	
				hospitalDatanew.setUser(hospitalData.getUser());
				hospitalDatanew.setName(hospitalData.getName());
				hospitalDatanew.setAddress(hospitalData.getAddress());
				hospitalDatanew.setOrganization(hospitalData.getOrganization());
				hospitalDatanew.setCity(hospitalData.getCity());
				hospitalDatanew.setCountry(hospitalData.getCountry());
				hospitalDatanew.setState(hospitalData.getState());
				hospitalDatanew.setLandLineNumber(hospitalData.getLandLineNumber());
				hospitalDatanew.setPinCode(hospitalData.getPinCode());
				hospitalDatanew.setpOC(hospitalData.getpOC());
				hospitalDatanew.setImage(hospitalData.getImage());
				hospitalDatanew.setDirectDial(hospitalData.getDirectDial());
				hospitalDatanew.setuRL(hospitalData.getuRL());
				session=sessionFactory.openSession();
				tx = session.beginTransaction();
			    id=(int) session.save(hospitalDatanew);
			    for(Hospitalbldunit hospitalunit :hospitalData.getHospitalbldunit()) {
			    	hospitalunit.setHospitalData(hospitalDatanew);
			    	hospitalunit.getHospitalData().setHospital_id(hospitalDatanew.getHospital_id());
			    	session.save(hospitalunit);
			    }
				tx.commit();
				tx=null;
			}catch(Exception e){
				System.out.println("Exception occured. "+e.getMessage());
				tx.rollback();
				throw new Exception(e.getMessage());
			}finally{
				System.out.println("Closing session");
				session.close();
			}
			return id;
	}

	@Override
	public void updateHospitalData(HospitalData hospitalData) {
		// TODO Auto-generated method stub
		Session session = null;
		Transaction tx = null;
		try{	
			session=sessionFactory.openSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(hospitalData);
			tx.commit();
			tx=null;
		}catch(Exception e){
			System.out.println("Exception occured. "+e.getMessage());
			tx.rollback();
			e.printStackTrace();
		}finally{
			System.out.println("Closing session");
			session.close();
		}
		
	}

	@Override
	public void deleteHospitalData(int hid) {
		// TODO Auto-generated method stub
		
	}
	
		
}
