package com.bbank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbank.dao.UserDao;
import com.bbank.model.User;


@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDao dao;

	@Autowired
    private PasswordEncoder passwordEncoder;
	
	public User findById(int id) {
		return dao.findById(id);
	}

	public User findFAByTypeId(String typeid) throws Exception {
		User user = dao.findFAByTypeId(typeid);
		return user;
	}
	
	public User findRMByTypeId(String typeid) throws Exception {
		User user = dao.findRMByTypeId(typeid);
		return user;
	}
	
	public User findByUserName(String username) {
		User user = dao.findByUserName(username);
		return user;
	}

	public void saveUser(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		dao.save(user);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateUser(User user) {
		User entity = dao.findById(user.getId());
		if(entity!=null){
			entity.setId(user.getId());
			if(user.getPassword() !=null && !user.getPassword().equals(entity.getPassword())){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setUsername(user.getUsername());
			entity.setEnabled(user.isEnabled());
			entity.setUsertype(user.getUsertype());
		}
		dao.updateUser(entity);
	}

	
	public void deleteByUserid(int id) {
		dao.deleteByUserid(id);
	}

	public List<User> findAllUsers() {
		return dao.findAllUsers();
	}

	public boolean isUserUserNameUnique(Integer id, String username) {
		User user = findByUserName(username);
		return ( user == null || ((id != null) && (user.getId() == id)));
	}

	@Override
	public User updateUserByName(User user) throws Exception {
		// TODO Auto-generated method stub
		User entity = dao.findByUserName(user.getUsername());
		if(entity!=null){
			//entity.setId(user.getId());
			if(!user.getPassword().equals(entity.getPassword())){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setUsername(user.getUsername());
			entity.setEnabled(user.isEnabled());
			entity.setUsertype(user.getUsertype());;
		}
	
		return dao.updateUserByName(entity);
	}

	@Override
	public User findUserNameType(String type, String username) {
		// TODO Auto-generated method stub
		User user=dao.findUserNameType(type, username);
		return user;
	}
	
}
