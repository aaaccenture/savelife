package com.bbank.service;

import java.util.List;

import com.bbank.model.BloodDetail;

public interface BloodetailService {
	
	void saveBloodDetail(BloodDetail bloodDetail) throws Exception;

	void updateBloodDetail(BloodDetail bloodDetail);

	BloodDetail findBloodDetailById(int id);

	List<BloodDetail> findAllBloodDetail();

	BloodDetail deleteBloodDetailById(int id);


}
