package com.bbank.service;

import java.util.List;

import com.bbank.model.DonorBloodunit;

public interface DonorBloodunitService {
	
	void saveDonorBloodunit(DonorBloodunit donorBloodunit) throws Exception;

	void updateDonorBloodunit(DonorBloodunit donorBloodunit);

	DonorBloodunit findDonorBloodunitById(int id);

	List<DonorBloodunit> findAllDonorBloodunit();

	DonorBloodunit deleteDonorBloodunitById(int id);

}
