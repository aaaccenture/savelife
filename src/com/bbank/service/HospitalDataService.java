package com.bbank.service;

import java.io.Serializable;
import java.util.List;

import com.bbank.model.HospitalData;



public interface HospitalDataService {
	
	    List<HospitalData> getallHospitalData();
	    HospitalData getHospitalDataById(int hid) throws Exception;
	    int addHospitalData(HospitalData hospitalData) throws Exception;
	    void updateHospitalData(HospitalData hospitalData) throws Exception;
	    void deleteHospitalData(int hid);
	   
}
