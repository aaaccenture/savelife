package com.bbank.service;

import java.util.List;

import com.bbank.model.User;


public interface UserService {
	
	User findById(int id);
	
	User findByUserName(String username);
	
    User findRMByTypeId(String typeid) throws Exception;
	
    User findFAByTypeId(String typeid) throws Exception;
	
	void saveUser(User user);
	
	void updateUser(User user);
	
	void deleteByUserid(int id);

	List<User> findAllUsers(); 
	
	User findUserNameType(String  type, String username);
	
	User updateUserByName(User user) throws Exception ;

}