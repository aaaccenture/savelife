package com.bbank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbank.dao.BloodDetailDAO;
import com.bbank.model.BloodDetail;

@Service("bloodetailService")
public class BloodDetailServiceImpl implements BloodetailService{

	@Autowired
	private BloodDetailDAO bloodDetailDAO;

	@Override
	public void saveBloodDetail(BloodDetail bloodDetail) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBloodDetail(BloodDetail bloodDetail) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BloodDetail findBloodDetailById(int id) {
		// TODO Auto-generated method stub
		
		return bloodDetailDAO.findBloodDetailById(id);
	}

	@Override
	public List<BloodDetail> findAllBloodDetail() {
		// TODO Auto-generated method stub
		 List<BloodDetail> bloodDetails=bloodDetailDAO.findAllBloodDetail();
		return bloodDetails;
	}

	@Override
	public BloodDetail deleteBloodDetailById(int id) {
		return null;
		// TODO Auto-generated method stub
		
	}
	
	
}
