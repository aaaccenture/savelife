package com.bbank.service;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bbank.dao.HospitalDataDAO;
import com.bbank.model.DonerProfile;
import com.bbank.model.HospitalData;


@Service("hospitalDataDAO")
public class HospitalDataServiceImpl implements HospitalDataService {
	@Autowired
	private HospitalDataDAO hospitalDataDAO;
	@Autowired
    private PasswordEncoder passwordEncoder;

	@Override
	public List<HospitalData> getallHospitalData() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HospitalData getHospitalDataById(int hid) throws Exception {
		// TODO Auto-generated method stub
		HospitalData hospitalData=hospitalDataDAO.getHospitalDataById(hid);
		return hospitalData;
}

	@Override
	public int addHospitalData(HospitalData hospitalData) throws Exception {
		// TODO Auto-generated method stub
		hospitalData.getUser().setPassword(passwordEncoder.encode(hospitalData.getUser().getPassword()));
	     
	return hospitalDataDAO.addHospitalData(hospitalData);
	}

	@Override
	public void updateHospitalData(HospitalData hospitalData) throws Exception {
		// TODO Auto-generated method stub
		HospitalData entity = hospitalDataDAO.getHospitalDataById(hospitalData.getHospital_id());
		if(entity!=null){
			entity.getUser().setId(hospitalData.getUser().getId());
			if(hospitalData.getUser().getPassword() !=null && !hospitalData.getUser().getPassword().equals(entity.getUser().getPassword())){
				entity.getUser().setPassword(passwordEncoder.encode(hospitalData.getUser().getPassword()));
			}
			entity.getUser().setUsername(hospitalData.getUser().getUsername());
			entity.getUser().setEnabled(hospitalData.getUser().isEnabled());
			entity.getUser().setUsertype(hospitalData.getUser().getUsertype());
			entity.getUser().setGroup(hospitalData.getUser().getGroup());
			entity.setHospital_id(hospitalData.getHospital_id());
			entity.setName(hospitalData.getName());
			entity.setOrganization(hospitalData.getOrganization());
			entity.setAddress(hospitalData.getAddress());
			entity.setCity(hospitalData.getCity());
			entity.setCountry(hospitalData.getCountry());
			entity.setState(hospitalData.getState());
			entity.setPinCode(hospitalData.getPinCode());
			entity.setLandLineNumber(hospitalData.getLandLineNumber());
			entity.setuRL(hospitalData.getuRL());
			entity.setImage(hospitalData.getImage());
			entity.setDirectDial(hospitalData.getDirectDial());
			
				
		}
		hospitalDataDAO.updateHospitalData(entity);
		

		
	}

	@Override
	public void deleteHospitalData(int cid) {
		// TODO Auto-generated method stub
		
	}
	
	
	}
