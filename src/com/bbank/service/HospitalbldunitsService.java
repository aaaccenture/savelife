package com.bbank.service;

import com.bbank.model.Hospitalbldunit;

public interface HospitalbldunitsService {
	
	int adddonorbldunit(Hospitalbldunit hospitalbldunit ) throws Exception;
	
	Hospitalbldunit gethospitalbldunitby(int hid,String bloodgroup);
	

}
