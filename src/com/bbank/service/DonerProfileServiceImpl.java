package com.bbank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbank.dao.DonerProfileDAO;
import com.bbank.model.DonerProfile;
import com.bbank.model.User;


@Service("donerProfileService")
@Transactional
public class DonerProfileServiceImpl implements DonerProfileService{

	@Autowired
	private DonerProfileDAO donerProfileDO;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Override
	public int saveDonerProfile(DonerProfile donerProfile) throws Exception {
		// TODO Auto-generated method stub
		donerProfile.getUser().setPassword(passwordEncoder.encode(donerProfile.getUser().getPassword()));
		return donerProfileDO.saveDonerProfile(donerProfile);
			
	}

	@Override
	public void updateDonerProfile(DonerProfile donerProfile) {
		// TODO Auto-generated method stub
		DonerProfile entity = null;
		try {
			entity = donerProfileDO.findByDonerProfileId(donerProfile.getDonorid());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(entity!=null){
			entity.getUser().setId(donerProfile.getUser().getId());
			if(donerProfile.getUser().getPassword() !=null && !donerProfile.getUser().getPassword().equals(entity.getUser().getPassword())){
				entity.getUser().setPassword(passwordEncoder.encode(donerProfile.getUser().getPassword()));
			}
			entity.getUser().setUsername(donerProfile.getUser().getUsername());
			entity.getUser().setEnabled(donerProfile.getUser().isEnabled());
			entity.getUser().setUsertype(donerProfile.getUser().getUsertype());
			entity.getUser().setGroup(donerProfile.getUser().getGroup());
			entity.setDonorid(donerProfile.getDonorid());
			entity.setAddress(donerProfile.getAddress());
			entity.setFirstName(donerProfile.getFirstName());
			entity.setLastname(donerProfile.getLastname());
			entity.setMiddle_name(donerProfile.getMiddle_name());
			entity.setFatherName(donerProfile.getFatherName());
			entity.setBloodGroup(donerProfile.getBloodGroup());
			entity.setAddress(donerProfile.getAddress());
			entity.setCountry(donerProfile.getCountry());
			entity.setCity(donerProfile.getCity());
			entity.setPinCode(donerProfile.getPinCode());
			entity.setProof_id(donerProfile.getProof_id());
			entity.setProofId_typ(donerProfile.getProofId_typ());
			entity.setBloodGroup(donerProfile.getBloodGroup());
			entity.setEmail(donerProfile.getEmail());
			entity.setMobile(donerProfile.getMobile());
			entity.setState(donerProfile.getState());
			entity.getUser().setEnabled(donerProfile.getUser().isEnabled());
			entity.setdOB(donerProfile.getdOB());
		}
		donerProfileDO.updateDonerProfile(entity);
		
	}

	@Override
	public DonerProfile findByDonerProfileId(int id) throws Exception {
		// TODO Auto-generated method stub
		
		return donerProfileDO.findByDonerProfileId(id);
	}

	@Override
	public List<DonerProfile> findAllDonerProfile() {
		// TODO Auto-generated method stub
		return donerProfileDO.findAllDonerProfile();
	}

	@Override
	public void deleteByDonerProfileid(int id) {
		// TODO Auto-generated method stub
		donerProfileDO.deleteByDonerProfileid(id);
		
	}

}
