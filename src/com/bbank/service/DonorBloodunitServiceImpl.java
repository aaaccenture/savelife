package com.bbank.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.bbank.dao.DonorBloodunitDAO;
import com.bbank.dao.HospitalDataDAO;
import com.bbank.dao.UserDao;
import com.bbank.model.DonerProfile;
import com.bbank.model.DonorBloodunit;
import com.bbank.model.HospitalData;
import com.bbank.model.User;
@Service("donorBloodunitService")
public class DonorBloodunitServiceImpl implements DonorBloodunitService{

	
	@Autowired
	private DonorBloodunitDAO donorBloodunitDAO;
	
	@Autowired
    private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDao dao;
	
	@Autowired
	private HospitalDataDAO hospitaldao;
	
	/*@Override
	public void saveDonorBloodunit(DonorBloodunit donorBloodunit) throws Exception {
		// TODO Auto-generated method stub
		DonerProfile entity = dao.findByUserName(donorBloodunit.getdo);
		if(entity!=null){
			entity.setId(donorBloodunit.getDonerProfile().getDonorid());
			if(donorBloodunit.getUser().getPassword()== null){
				entity.setPassword(passwordEncoder.encode(entity.getPassword()));
			}
			
		}
		donorBloodunit.setUser(entity);
		HospitalData hospital=hospitaldao.getHospitalDataById(donorBloodunit.getHospitalData().getHospital_id());
		hospital.setUser(entity);
		donorBloodunit.setHospitalData(hospital);
		donorBloodunitDAO.saveDonorBloodunit(donorBloodunit);
	}

*/	@Override
	public void updateDonorBloodunit(DonorBloodunit donorBloodunit) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DonorBloodunit findDonorBloodunitById(int id) {
		// TODO Auto-generated method stub
		DonorBloodunit donorBloodunit=donorBloodunitDAO.findDonorBloodunitById(id);
		return donorBloodunit;
	}

	@Override
	public List<DonorBloodunit> findAllDonorBloodunit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DonorBloodunit deleteDonorBloodunitById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveDonorBloodunit(DonorBloodunit donorBloodunit) throws Exception {
		// TODO Auto-generated method stub
		donorBloodunitDAO.saveDonorBloodunit(donorBloodunit);
		
	}

}
