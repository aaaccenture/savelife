<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Quikk Loan :: Admin</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
		<link rel="icon" href="favicon.ico" type="image/x-icon" />
		<link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Roboto' />
        <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700|Josefin+Sans' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type='text/css' href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- inject:css -->				
		<link rel="stylesheet" href="app/assets/css/main.min.css">				
		<!-- endinject -->	
		<!-- inject:js -->
		<script src="app/scripts/angular.min.js"></script>
		<script src="app/scripts/vendors.min.js"></script>
		<script src="app/scripts/main.min.js"></script>
		<!-- endinject -->
	</head>
	<body ng-app="ax">
		<div class="loading-spiner-holder" data-loading >
	        <div class="loading-spiner">
	            <img src="app/assets/images/loading.svg" />
	        </div>
	    </div>
		<flash-message
			name="flash-fixed",
		    class="flash-fixed"
		></flash-message>		
		<div ui-view="header"></div>
		<div ui-view="content"></div>
		<div ui-view="footer"></div>
	</body>
</html>